import style from './modal.module.scss'
import PropTypes from 'prop-types';

export default function Modal({ data: { type, title, description, closeBtn = false, closeBtnHandler, actions }, onCancel, onSubmit }) {

    return (
        <div className={style.modal + ' ' + style[type]} onClick={onCancel}>
            <div className={style.modal__content} onClick={(e) => e.stopPropagation()}>
                <header className={style.modal__header}>
                    <h3 className={style.modal__title}>
                        {title}
                    </h3>
                    {closeBtn && closeBtnHandler(onCancel, style.close)}
                </header>
                <div className={style.modal__body}>
                    {description && <p>{description}</p>}
                    {actions && actions(onCancel, onSubmit, style.modal__btns)}
                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    closeBtn: PropTypes.bool,
    data: PropTypes.shape({
        type: PropTypes.string,
        title: PropTypes.string,
        closeBtn: PropTypes.bool,
        closeBtnHandler: PropTypes.func,
        description: PropTypes.string,
        actions: PropTypes.func,
    })
}