export const modalProps = [
    {
        type: 'buy',
        title: "Додати в кошик?",
        description: 'Ви дійсно хочете додати цей товар у ваш кошик?',
        closeBtn: true,
        closeBtnHandler(onCloseHandler, className) {
            return (
                <button onClick={onCloseHandler} className={className} />
            )
        },
        actions(onCloseHandler, onSubmitHandler, className) {
            return (
                <div className={className}>
                    <button onClick={onSubmitHandler} className='submit'>Додати</button>
                    <button onClick={onCloseHandler} className='cancel'>Відміна</button>
                </div>
            )
        },
    },
    {
        type: 'delete',
        title: "Видалити з кошика?",
        description: 'Ви дійсно хочете видалити цей товар з вашого кошика?',
        closeBtn: false,
        closeBtnHandler(onCloseHandler, className) {
            return (
                <button onClick={onCloseHandler} className={className} />
            )
        },
        actions(onCloseHandler, onSubmitHandler, className) {
            return (
                <div className={className}>
                    <button onClick={onSubmitHandler} className='submit'>Видалити</button>
                    <button onClick={onCloseHandler} className='cancel'>Відміна</button>
                </div>
            )
        },
    }
]
