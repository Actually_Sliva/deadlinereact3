import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import style from './notFound.module.scss'

export function NotFound() {
    const navigate = useNavigate();

    return (
        <>
            <div className={style.notFound}>
                <div className={style.notFound__container}>
                    <div className={style.notFound__wrapper}>
                        <p className={style.notFound__subtitle}>404</p>
                        <h1 className={style.notFound__title}>Сторінка не знайдена</h1>
                        <p className={style.notFound__message}>
                            Ви можете повернутись на
                            <Link to="/" className={style.notFound__link}>Головну сторінку</Link>
                        </p>
                        <p className={style.notFound__message}>
                            Або повернутись
                            <Link onClick={() => navigate(-1)} className={style.notFound__link}>Назад</Link>
                        </p>
                    </div>
                </div>
            </div>
        </>
    )
}