import { useOutletContext } from "react-router-dom";
import ProductList from '../../components/ProductList';

export function Home() {
    const { products, error, loading } = useOutletContext()

    if (error) {
        return <p>Відбулась непередбаченна помилка при завантаженні, спробуйте оновити сторінку</p>;
    }

    return (
        <>
            {!loading
                ? <ProductList products={products} type={'buy'} />
                : <p>Йде завантаження...</p>
            }
        </>
    )
}