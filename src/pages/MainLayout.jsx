import { useEffect, useState } from 'react'
import Modal from "../components/Modal";
import { modalProps } from '../components/Modal/modalProps.jsx'
import Header from '../components/Header';
import { Outlet } from 'react-router-dom';
import { fetchData, getDataFromLS } from '../Utils';

export function MainLayout() {
    const [modalType, setModalType] = useState(null)
    const [artNum, setArtNum] = useState(null)

    const [products, setProducts] = useState(getDataFromLS('products'))
    const [loading, setLoading] = useState(null)
    const [error, setError] = useState(null)

    useEffect(() => {
        setLoading(true);
        (async () => {
            try {
                const data = await fetchData('/products.json')
                setProducts(data);
                setLoading(false);
            } catch (error) {
                setLoading(false);
                setError(error);
            }
        })()
    }, []);

    const [favorites, setFavorites] = useState(getDataFromLS('favorites'))
    const [cart, setCart] = useState(getDataFromLS('cart'))
    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites))
        localStorage.setItem('cart', JSON.stringify(cart))
    }, [favorites, cart])

    function openModalHandler(e, type, artNum) {
        e.preventDefault();
        setModalType(type)
        setArtNum(artNum)
    }

    function closeModalHandler() {
        setModalType(null)
        setArtNum(null)
    }

    function addToFavoriteHandler(e, product) {
        e.preventDefault();

        favorites.find(favorite => favorite.artNum === product.artNum)
            ? setFavorites([...favorites.filter(favorite => favorite.artNum !== product.artNum)])
            : setFavorites([...favorites, product])
    }

    function onSubmiteCartHandler() {
        if (modalType === 'buy') {
            addToCart()
        } else if (modalType === 'delete') {
            deleteFromCart()
        }
        closeModalHandler()
    }

    function addToCart() {
        const newItem = products.find(product => product.artNum === artNum)
        setCart([...cart, newItem])
    }

    function deleteFromCart() {
        const oldItem = products.find(product => product.artNum === artNum)
        const oldItemIndex = cart.lastIndexOf(cart.find(item => item.artNum === oldItem.artNum))
        setCart([...cart.slice(0, oldItemIndex), ...cart.slice(oldItemIndex + 1)])
    }

    return (
        <>
            {modalType
                && <Modal
                    onSubmit={onSubmiteCartHandler}
                    onCancel={closeModalHandler}
                    data={modalProps.find(modal => modal.type === modalType)} />}
            <Header
                favorites={favorites.length}
                cart={cart.length} />
            <Outlet context={{
                products,
                loading,
                error,
                favorites,
                cart,
                addToFavoriteHandler,
                openModalHandler,
            }} />
        </>
    )
}